## Parte básica
Al parecer por una extraña razón que desconozco no se pueden ver los paquetes correspondientes en las capturas de wireshark.

## Parte adicional
* Peticiones concurrentes

Lanzando dos clientes distintos:

    -python3 client.py 127.0.0.1:6001 sip:yoa@clientes.net sip:cancion@songs.net 10 received2a.mp3
    -python3 client2.py 127.0.0.1:6001 sip:yob@clientes.net sip:cancion@songs.net 10 received2b.mp3


* Cabecera de tamaño


Se puede observar en el código de **client.py** en **invite_method**

![](cabecera_tamaño.png)



* Gestión de errores 

Se han incluido los correspondientes códigos de error, en lugar de **200 OK**.
¿Como visualizarlos? Haciendo errores :)
