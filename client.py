#!/usr/bin/python3
# -*- coding: utf-8 -*-
""" Cliente """

import socketserver
import sys
import socket
import logging
import threading

# Tiempo.
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%Y%m%d%H%M%S')

# Constantes.
SERVER = "127.0.0.1"
PORT = 34543
ERROR_MESSAGE = "Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <time> <file>"
SIP_CONSTANT = "SIP/2.0"
REGISTER = "REGISTER"

STATUS_CODES = {
    200: b"SIP/2.0 200 OK",
    400: b"SIP/2.0 400 Bad Request",
    405: b"SIP/2.0 405 Method Not Allowed"
}


def invite_method(address_server_rtp, address_client):
    method = f"INVITE {address_server_rtp} {SIP_CONSTANT}\r\n"
    head = "Content-Type: application/sdp\r\n"
    body = f"v=0\no={address_client} 127.0.0.1\ns={address_server_rtp.split('@')[0]}\nt=0\nm=audio {PORT} RTP"
    length = f"Content-Length: {len(body)}\r\n"
    return method + head + length + body


def ack_method(address_server_rtp, address_client):
    method = f"ACK {address_server_rtp} {SIP_CONSTANT}\r\n"
    return method


def bye_method(address_server_rtp, address_client):
    method = f"BYE {address_server_rtp} {SIP_CONSTANT}\r\n"
    return method


SIP_REQUEST = {
    "INVITE": invite_method,
    "ACK": ack_method,
    "BYE": bye_method
}


def get_application_parameters():
    try:
        server_sip_ip, server_sip_port = sys.argv[1].split(":")
        address_client = sys.argv[2]
        address_server_rtp = sys.argv[3]
        time_expire = sys.argv[4]
        audio_file = sys.argv[5]
        return server_sip_ip, int(server_sip_port), address_client, address_server_rtp, time_expire, audio_file
    except Exception:
        sys.exit(ERROR_MESSAGE)


def register_client_in_sip(server_sip_ip, server_sip_port, address_client):
    try:
        client_request = f"{REGISTER} {address_client} {SIP_CONSTANT}\r\n\r\n"
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as client_socket:
            client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            client_socket.bind((SERVER, PORT))
            client_socket.connect_ex((server_sip_ip, server_sip_port))
            client_socket.send(client_request.encode('utf-8'))
            logging.info(f"SIP to {server_sip_ip}:{server_sip_port}: {repr(client_request)}")
            sip_response = client_socket.recv(1024).decode("utf-8")
            logging.info(f"SIP from {server_sip_ip}:{server_sip_port}: {repr(sip_response)}")
            return sip_response
    except Exception as e:
        sys.exit(f"Program Error: {e}")


def invite_client_in_sip(address_client, address_server_rtp, server_sip_ip, server_sip_port):
    try:
        client_request = SIP_REQUEST["INVITE"](address_server_rtp, address_client)
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as client_socket:
            client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            client_socket.connect_ex((server_sip_ip, server_sip_port))
            client_socket.send(bytes(client_request, "utf-8"))
            logging.info(f"SIP to {server_sip_ip}:{server_sip_port}: {repr(client_request)}")
            sip_response = client_socket.recv(1024).decode("utf-8")
            logging.info(f"SIP from {server_sip_ip}:{server_sip_port}: {repr(sip_response)}")
            return sip_response
    except Exception as e:
        sys.exit(f"Program Error: {e}")


def ack_client_in_sip(address_client, address_server_rtp, server_sip_ip, server_sip_port):
    try:
        client_request = SIP_REQUEST["ACK"](address_server_rtp, address_client)
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as client_socket:
            client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            client_socket.connect_ex((server_sip_ip, server_sip_port))
            client_socket.send(bytes(client_request, "utf-8"))
            logging.info(f"SIP to {server_sip_ip}:{server_sip_port}: {repr(client_request)}")
            sip_response = client_socket.recv(1024).decode("utf-8")
            logging.info(f"SIP from {server_sip_ip}:{server_sip_port}: {repr(sip_response)}")
            return sip_response
    except Exception as e:
        sys.exit(f"Program Error: {e}")


def bye_client_in_sip(address_client, address_server_rtp, server_sip_ip, server_sip_port, serv):
    try:
        client_request = SIP_REQUEST["BYE"](address_server_rtp, address_client)
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as client_socket:
            client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            client_socket.connect_ex((server_sip_ip, server_sip_port))
            client_socket.send(bytes(client_request, "utf-8"))
            logging.info(f"SIP to {server_sip_ip}:{server_sip_port}: {repr(client_request)}")
            sip_response = client_socket.recv(1024).decode("utf-8")
            logging.info(f"SIP from {server_sip_ip}:{server_sip_port}: {repr(sip_response)}")
        serv.socket.detach()
        serv.server_close()
    except Exception as e:
        sys.exit(ERROR_MESSAGE)


def write_in_file(file_name: str, content):
    with open(file_name, "w+b") as f:
        f.write(content)


def get_audio_file_name():
    server_sip_ip, server_sip_port, address_client, address_server_rtp, time_expire, audio_file = get_application_parameters()
    return audio_file


class EchoHandler(socketserver.DatagramRequestHandler):

    def handle(self):
        try:
            rtp_packages = self.rfile.read()
            server_ip = self.client_address[0]
            server_port = self.client_address[1]
            logging.debug(f"RTP packages from : {server_ip}:{server_port}")
            audio_file = get_audio_file_name()
            write_in_file(audio_file, rtp_packages)
        except Exception as e:
            self.wfile.write(STATUS_CODES[405])


if __name__ == '__main__':
    logging.debug(f"=========== Starting Client =============\n")
    logging.info(f"Starting...\n")
    server_sip_ip, server_sip_port, address_client, address_server_rtp, time_expire, audio_file = get_application_parameters()
    sip_response = register_client_in_sip(server_sip_ip, server_sip_port, address_client)
    serv = socketserver.UDPServer((server_sip_ip, PORT), EchoHandler)
    if "200" in sip_response:
        sip_response = invite_client_in_sip(address_client, address_server_rtp, server_sip_ip, server_sip_port)
        if "200" in sip_response:
            sip_response = ack_client_in_sip(address_client, address_server_rtp, server_sip_ip, server_sip_port)
        else:
            sys.exit(ERROR_MESSAGE)

        threading.Timer(
            float(time_expire),
            bye_client_in_sip,
            kwargs={
                "address_client": address_client,
                "address_server_rtp": address_server_rtp,
                "server_sip_ip": server_sip_ip,
                "server_sip_port": server_sip_port,
                "serv": serv
            }
        ).start()

        threading.Timer(
            float(time_expire),
            register_client_in_sip,
            kwargs={
                "server_sip_ip": server_sip_ip,
                "server_sip_port": server_sip_port,
                "address_client": address_client
            }
        ).start()
        logging.info(f"RTP ready {PORT}")
        logging.debug(f"===========SIP Server Listening in address {SERVER}:{PORT}=============\n")
        serv.serve_forever()
    else:
        sys.exit(ERROR_MESSAGE)
