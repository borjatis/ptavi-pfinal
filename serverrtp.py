#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socketserver
import sys
import socket
import os
import simplertp
import logging
import random
import threading

# Tiempo.
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s-%(message)s', datefmt='%Y%m%d%H%M%S')

# Constantes.
RTP_IP = "127.0.0.1"
RTP_PORT = 9999
RTP_ERROR_MESSAGE = "Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service> <file>"
SIP_ERROR_MESSAGE = "Server SIP invalid response.Exit to program"
SIP_CONSTANT = "SIP/2.0"
REGISTER = "REGISTER"
RTP_DOMAIN = "@songs.net"

RTP_METHODS = ["INVITE", "ACK", "BYE"]

STATUS_CODES = {
    200: b"SIP/2.0 200 OK",
    400: b"SIP/2.0 400 Bad Request",
    405: b"SIP/2.0 405 Method Not Allowed"
}


def _find(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            return True


def get_application_parameters():
    try:
        if len(sys.argv) == 3 and _find(sys.argv[2], "."):
            audio_file = sys.argv[2]
            ip_server_sip, port_server_sip = sys.argv[1].split(":")
            return ip_server_sip, int(port_server_sip), audio_file
        else:
            sys.exit(RTP_ERROR_MESSAGE)
    except Exception:
        sys.exit(RTP_ERROR_MESSAGE)


def register_rtp_in_sip(server_sip_ip, server_sip_port, audio_file):
    try:
        rtp_address_to_register_in_sip = " sip:" + audio_file.split(".")[0] + RTP_DOMAIN
        rtp_request = REGISTER + rtp_address_to_register_in_sip + f" {SIP_CONSTANT}\r\n\r\n"

        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as rtp_socket:
            rtp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            rtp_socket.bind((RTP_IP, RTP_PORT))
            rtp_socket.connect_ex((server_sip_ip, server_sip_port))
            rtp_socket.send(rtp_request.encode('utf-8'))
            logging.info(f"SIP to {server_sip_ip}:{server_sip_port}: {repr(rtp_request)}")
            sip_response = rtp_socket.recv(1024).decode("utf-8")
            logging.info(f"SIP from {server_sip_ip}:{server_sip_port}: {repr(sip_response)}")
            rtp_socket.close()
            return sip_response
    except Exception as e:
        sys.exit(RTP_ERROR_MESSAGE)


def get_params_of_document_sdp_body(sip_document_sdp_body):
    version, origin, session, time_session, extra = sip_document_sdp_body.split("\n")
    version = int(version.split("=")[1])
    client_address, client_ip = origin.split()
    client_address = client_address.split(":")[1]
    time_session = int(time_session.split("=")[1])
    type_element, client_port, connection_type = extra.split()
    return client_address, client_ip, client_port


def send_audio_file(client_ip, client_port):
    csrc = []
    for i in range(8):
        csrc.append(random.randint(2, 70))
    RTP_header = simplertp.RtpHeader()
    RTP_header.set_header()
    RTP_header.setCSRC(csrc)
    payloads = simplertp.RtpPayloadMp3(audio_file)
    simplertp.send_rtp_packet(RTP_header, payloads, client_ip, client_port)


class EchoHandler(socketserver.DatagramRequestHandler):
    rtp_session_data = {}

    def handle(self):
        try:
            sip_request = self.rfile.read().decode("utf-8")
            request_ip = self.client_address[0]
            request_port = self.client_address[1]
            logging.debug(f"###### Start connection to {request_ip}:{request_port} ######\n")
            logging.info(f"SIP from {request_ip}:{request_port}: {repr(sip_request)}\n")
            if RTP_METHODS[0] in sip_request:
                logging.debug(f"{sip_request}")
                method, head, length, body = sip_request.split("\r\n")
                client_address, client_ip, client_port = get_params_of_document_sdp_body(body)
                server_response = STATUS_CODES[200]
                self.wfile.write(server_response)
                logging.info(f"SIP to {request_ip}:{request_port}: {repr(server_response)}\n")
                send_audio_file(client_ip, int(client_port))
            elif RTP_METHODS[2] in sip_request:
                logging.debug(f"{sip_request}")
                server_response = STATUS_CODES[200]
                self.wfile.write(server_response)
                logging.info(f"SIP to {request_ip}:{request_port}: {repr(server_response)}\n")
                logging.debug(f"\tInterruption of sending RTP packets to client")
            else:
                server_response = b"Request wrong"
                self.wfile.write(server_response)
                logging.info(f"SIP to {request_ip}:{request_port}: {repr(server_response)}\n")
            logging.debug(f"###### Finish connection to {request_ip}:{request_port} ######\n")
        except Exception as e:
            self.wfile.write(STATUS_CODES[405])


if __name__ == '__main__':
    try:
        # Cliente RTP.
        server_sip_ip, server_sip_port, audio_file = get_application_parameters()
        logging.debug(f"===========RTP Server Starting=============\n")
        sip_response = register_rtp_in_sip(server_sip_ip, server_sip_port, audio_file)

        threading.Timer(
            float(10),
            register_rtp_in_sip,
            kwargs={
                "server_sip_ip": server_sip_ip,
                "server_sip_port": server_sip_port,
                "audio_file": audio_file
            }
        ).start()
        if "200" in sip_response:
            # server RTP
            serv = socketserver.UDPServer((server_sip_ip, RTP_PORT), EchoHandler)
            logging.debug(f"=======RTP Server Listening in address {RTP_IP}:{RTP_PORT}=========\n")
            serv.serve_forever()
        else:
            sys.exit(SIP_ERROR_MESSAGE)
    except Exception as e:
        sys.exit(f"Program Error: {e}")
