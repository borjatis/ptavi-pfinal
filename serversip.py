#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socketserver
import sys
import socket
import json
import logging

# Tiempo.
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%Y%m%d%H%M%S')

# Constantes.
SIP_ERROR_MESSAGE = "Usage: python3 serversip.py <port>"
SIP_IP = "127.0.0.1"
FILE_PATH = "registrar.json"

SIP_METHODS = ["REGISTER", "INVITE", "ACK", "BYE"]

STATUS_CODES = {
    200: b"SIP/2.0 200 OK",
    400: b"SIP/2.0 400 Bad Request",
    405: b"SIP/2.0 405 Method Not Allowed"
}


def get_application_parameters():
    try:
        server_sip_port = sys.argv[1]
        return int(server_sip_port)
    except Exception:
        sys.exit(SIP_ERROR_MESSAGE)


def find_hist_register_data(register_data_file):
    try:
        with open(register_data_file, encoding="utf8") as r:
            data = json.load(r)
            SIPRegisterHandler.sip_session_data = data
        logging.debug(f"User history found {data}\n")
    except Exception:
        logging.debug("User history not found\n")


def write_in_json_file(file_name: str, content):
    with open(file_name, "w") as f:
        json.dump(content, f)


def remove_registers():
    logging.debug(f"Data: {str(SIPRegisterHandler.sip_session_data)}")
    SIPRegisterHandler.sip_session_data = {}
    logging.debug("All Data remove")


def sip_proxy(ip, port, request):
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sip_socket:
            sip_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sip_socket.connect((ip, port))
            logging.info(f"SIP to {ip}:{port}: {request}")
            sip_socket.send(request.encode('utf-8'))
            response = sip_socket.recv(1024)
            logging.info(f"SIP from {ip}:{port}: {repr(response)}")
            return response
    except Exception as e:
        sys.exit(f"Program Error: {e}")


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    sip_session_data = {}

    def handle(self):
        try:
            sip_request = self.rfile.read().decode("utf-8")
            request_ip = self.client_address[0]
            request_port = self.client_address[1]
            logging.debug(f"###### Start connection to {request_ip}:{request_port} ######\n")
            logging.info(f"SIP from {request_ip}:{request_port}: {repr(sip_request)}\n")
            if SIP_METHODS[0] in sip_request:
                request_address = sip_request.split()[1].split(":")[1]
                self.sip_session_data[request_address] = (request_ip, request_port)
                logging.debug(f"Register: {self.sip_session_data}")
                write_in_json_file(FILE_PATH, self.sip_session_data)
                self.wfile.write(STATUS_CODES[200])
                logging.info(f"SIP to {request_ip}:{request_port}: {repr(STATUS_CODES[200])}\n")
            elif SIP_METHODS[1] in sip_request:
                logging.debug(f"{sip_request}")
                method, head, length, body = sip_request.split("\r\n")
                method, server_rtp_address, sip_constant = method.split()
                server_rtp_address = server_rtp_address.split(":")[1]
                server_rtp_ip, server_rtp_port = self.sip_session_data[server_rtp_address]
                server_response = sip_proxy(server_rtp_ip, server_rtp_port, sip_request)
                self.wfile.write(server_response)
                logging.info(f"SIP to {request_ip}:{request_port}: {repr(server_response)}\n")
            elif SIP_METHODS[2] in sip_request:
                logging.debug(f"{sip_request}")
                method, server_rtp_address, sip_constant = sip_request.split()
                server_rtp_address = server_rtp_address.split(":")[1]
                server_response = STATUS_CODES[200]
                self.wfile.write(server_response)
                logging.info(f"SIP to {request_ip}:{request_port}: {repr(server_response)}\n")
            elif SIP_METHODS[3] in sip_request:
                logging.debug(f"{sip_request}")
                method, server_rtp_address, sip_constant = sip_request.split()
                server_rtp_address = server_rtp_address.split(":")[1]
                server_rtp_ip, server_rtp_port = self.sip_session_data[server_rtp_address]
                server_response = sip_proxy(server_rtp_ip, server_rtp_port, sip_request)
                self.wfile.write(server_response)
                logging.info(f"SIP to {request_ip}:{request_port}: {repr(server_response)}\n")
            else:
                server_response = b"Request wrong"
                self.wfile.write(server_response)
                logging.info(f"SIP to {request_ip}:{request_port}: {repr(server_response)}\n")
            logging.debug(f"###### Finish connection to {request_ip}:{request_port} ######\n")
        except Exception as e:
            self.wfile.write(STATUS_CODES[405])


if __name__ == "__main__":
    try:
        server_sip_port = get_application_parameters()
        logging.debug(f"===========SIP Server Starting=============\n")
        logging.info(f"Starting...\n")
        find_hist_register_data(FILE_PATH)
        serv = socketserver.UDPServer((SIP_IP, server_sip_port), SIPRegisterHandler)
        logging.debug(f"===========SIP Server Listening in address {SIP_IP}:{server_sip_port}=============\n")
        serv.serve_forever()
    except Exception as e:
        sys.exit(f"Program Error: {e}")
